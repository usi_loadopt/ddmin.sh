#!/bin/bash

if [ $# -ne 2 ]; then
	echo "USAGE: ./ddmin.sh program input"
	exit 192
fi

logger="/tmp/ddmin.log"

program=$1
input=$2

ddmin()
{
	local a=$1	# begin of the input
	local b=$2	# end of the input
	local g=$3	# number of subsets (granularity)

	echo "-----------------------------" > $logger
	echo "GRANULARITY is $g" > $logger
	echo "-----------------------------" > $logger

	subset_a=$a
	let 'subset_size = (b - a) / g'
	let 'subset_b = subset_a + subset_size'

	while true; do
		if [ $subset_b -gt $b ]; then
			subset_b=$b
		fi
	
		let 'skip = subset_a - 1'
		let 'count = subset_b - subset_a + 1'
	
		dd if=$input of=/tmp/subset bs=1 skip=$skip count=$count &> /dev/null 

		$program < /tmp/subset

		if [ $? -eq $orig_fail ]; then
			minimal_subset=$(cat /tmp/subset)
			echo "The following input (a=$a,b=$b) triggers the bug:" > $logger
			echo "---BEGIN---" > $logger
			cat /tmp/subset > $logger
			echo -e "\n---END---" > $logger
			if [ $subset_a -ne $subset_b ]; then
				ddmin $subset_a $subset_b 2
			fi
			return
		fi 	
		echo -e "\n$subset_a -> $subset_b" > $logger
	
		if [ $subset_b -eq $b ]; then
			break;
		fi

		let 'subset_a = subset_b + 1'
		let 'subset_b = subset_a + subset_size'
	done

	if [ $subset_size -ne 0 ]; then
		let 'new_g = g + 1'
		ddmin $a $b $new_g
	fi
}

$program < $input > $logger

orig_fail=$?

echo "Original failure: $orig_fail"

size=`wc -c < $input` 
a=1
let 'b = size'

minimal_subset=$(cat $input)
ddmin $a $b 2

echo "Minimal subset:"
echo $minimal_subset


