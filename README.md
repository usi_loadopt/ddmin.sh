# Description

ddmin.sh works with programs that read the input from the
standard input. For the sake of simplicity, by failure
I use the exit code of a program. If given two inputs A and B
the program exits with the same code then I assume that A
and B trigger the same defect.
In a more general solution I could use gdb to dump the stack trace
and check if I am triggering the same failure.

As input I use always the following file:

    cat input.txt
    0abcdefghilmnopqrstuvz

# Example 1

buggy1.sh is a program that crashes (exit code 192) if the
input contains the string "mno"

    ./ddmin.sh ./buggy1.sh input.txt
    Original failure: 192
    Minimal subset:
    mno

# Example 2

buggy2.sh is a program that crashes (exit code 192) if the
input contains the string "0abcdefghilmnopqrstuvz"

    ./ddmin.sh ./buggy2.sh input.txt
    Original failure: 192
    Minimal subset:
    0abcdefghilmnopqrstuvz

# Example 3

buggy3.sh is a program that crashes (exit code 192) for any input

    ./ddmin.sh ./buggy3.sh input.txt
    Original failure: 192
    Minimal subset:
    0

